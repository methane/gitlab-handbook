---
title: "GitLab Data Processing Addendum and Standard Contractual Clauses"
---

[GitLab Data Processing Addendum](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Customer_DPA__3.1.23_.pdf)
[Exhibit B - Standard Contractual Clauses](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Exhibit_B_-_Standard_Contractual_Clauses__10.13.22_.pdf)
